db = new Mongo().getDB("admin");

db.createUser({
  user: "adminCluster",
  pwd: "secret-pass",
  roles: [{
    role: "clusterAdmin",
    db: "admin"
  }]
});

db.auth("adminCluster", "secret-pass");

db = db.getSiblingDB("test");

db.createUser({
  user: "test",
  pwd: "test123",
  roles: [{
    role: "dbOwner",
    db: "test"
  }]
});