import Vue from "vue";
import Cookie from "js-cookie";

export default {
  namespaced: true,
  state() {
    return {
      token: null
    };
  },
  getters: {
    isAuth(state) {
      return state.token != null
    },
    token(state) {
      return state.token
    }
  },
  mutations: {
    set_token(state, data) {
      state.token = data
    }
  },
  actions: {
    async login(store, data) {
      try {
        const resp = await this.$axios.$get(
          `/auth?name=${data.name}&password=${data.password}`
        );
        localStorage.setItem("token", resp.token)
        Cookie.set("token", resp.token)
        store.commit("set_token", resp.token)
      } catch (error) {
        return Promise.reject(error)
      }
    },
    logout(store, data) {
      Cookie.remove("token")
      localStorage.removeItem("token")
      store.commit("set_token", null)
    },
    initAuth(store, req) {
      if (req) {
        if (!req.headers.cookie) {
          return
        }
        let token;
        token = req.headers.cookie.split(';').find(t => t.trim().startsWith('token=')).split('=')[1]
        store.commit("set_token", token)
      } else {
        let token = localStorage.getItem("token")
        store.commit("set_token", token)
      }

    }
  }
};
