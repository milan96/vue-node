const mongoose = require("mongoose")
const Schema = mongoose.Schema

const textSchema = new Schema({
    text: String,
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
})

module.exports = mongoose.model("Text", textSchema)