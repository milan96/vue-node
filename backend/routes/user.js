const express = require("express")
const router = express.Router()
const UserController = require("../controllers/user")
const isAuth = require("../middleware/is-auth")

router.post('/', UserController.signup)

router.get('/', isAuth, UserController.getUser)

module.exports = router;