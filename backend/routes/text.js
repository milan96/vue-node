const express = require("express")
const router = express.Router()
const TextController = require("../controllers/text")
const isAuth = require("../middleware/is-auth")

router.post('/', TextController.createText)
router.get('/mytexts', TextController.getMyTexts)

module.exports = router;