const User = require("../models/user")
const Text = require("../models/text")
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken')

exports.createText = (req, res, next) => {

    const token = req.query.token

    let decodedToken
    try {
        decodedToken = jwt.verify(token, 'secretsuperbigsecret')
    } catch (error) {
        error.statusCode = 500;
        throw error
    }

    if (!decodedToken) {
        const error = new Error('Not authenticated')
        error.statusCode = 401;
        throw error
    }

    const text = new Text({
        text: req.body.text,
        userId: decodedToken.userId
    })
    text.save()
    res.status(200).json({ status: 'OK' })
}

exports.getMyTexts = (req, res, next) => {

    const token = req.query.token

    let decodedToken
    try {
        decodedToken = jwt.verify(token, 'secretsuperbigsecret')
    } catch (error) {
        error.statusCode = 500;
        throw error
    }

    if (!decodedToken) {
        const error = new Error('Not authenticated')
        error.statusCode = 401;
        throw error
    }

    Text.find({ userId: decodedToken.userId }).then(texts => {
        console.log(res);
        res.status(200).json({ texts: texts })
    })
}