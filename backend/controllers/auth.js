const User = require("../models/user")
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken')

exports.login = (req, res, next) => {
    console.log(req.query);
    const name = req.query.name
    const password = req.query.password
    let user;
    User.findOne({ name: name }).then(usr => {
        if (!usr) {
            const error = new Error('User with that name is not exists')
            error.statusCode = 404
            throw error
        }
        user = usr
        return bcrypt.compare(password, user.password)
    }).then(isPwd => {
        if (!isPwd) {
            const error = new Error('Password is wrong')
            error.statusCode = 400
            throw error
        } else {
            const token = jwt.sign({ userId: user._id.toString() },
                'secretsuperbigsecret', {})
            res.status(200).json({ token: token, id: user._id.toString() })
        }
    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })

}