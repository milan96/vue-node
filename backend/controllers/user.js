const User = require("../models/user")
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken')

exports.signup = (req, res, next) => {
    const name = req.body.name
    const password = req.body.password
    let created_user;

    bcrypt.hash(password, 12).then(hp => {
        const user = new User({
            name: name,
            password: hp
        })


        created_user = user
        return user.save()
    }).then(result => {
        const token = jwt.sign({ userId: created_user._id.toString() },
            'secretsuperbigsecret', {})
        res.status(200).json({ message: "Success", userId: result._id, token: token })
    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })
}

exports.getUser = (req, res, next) => {

    const token = req.query.token

    let decodedToken
    try {
        decodedToken = jwt.verify(token, 'secretsuperbigsecret')
    } catch (error) {
        error.statusCode = 500;
        throw error
    }

    if (!decodedToken) {
        const error = new Error('Not authenticated')
        error.statusCode = 401;
        throw error
    }

    User.findById(decodedToken.id).then(usr => {
        let resp = usr.toObject()
        delete resp.password
        res.status(200).json({ user: resp })
    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })
}