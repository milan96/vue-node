const express = require("express")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const app = express()

app.use(bodyParser.json())

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
})

const userRoute = require("./routes/user")
const authRoute = require("./routes/auth")
const textRoute = require("./routes/text")

app.use('/api/user', userRoute)
app.use('/api/auth', authRoute)
app.use('/api/text', textRoute)

app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500
    const message = error.message
    const data = error.data

    res.status(status).json({ message: message, data: data })
    next();
})

mongoose.connect('mongodb://test:test123@mongo:27017/test', { useNewUrlParser: true, useUnifiedTopology: true }).then(res => {
    console.log('res');
    app.listen(8080)
}).catch(err => {
    console.log(err);
})
